# Demo of DIME collaboration

Using public data we will show:

1. A DIME 'contributor' creates a branch, makes a change and commits and pushes this to the project.
2. When they are finished, they create a 'merge request' (called a pull request on GitHub)
3. A repo maintainer will check the code works, and provide feedback.
4. The 'contributor' addresses the comments and makes a new merge request
5. The repo maintainer approves the merge request and merges the branch with the main.